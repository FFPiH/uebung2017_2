Aufgabe 1
=========

> module Aufgabe1 where

Functional phone book – Implementieren Sie ein Telefonbuch als Funktion.

> type Number = String
> type Name = String
> type Entry = [Number]
> newtype PhoneBook = PB (Name -> Entry)


Implementieren Sie die Funktion `usePB`, welche aus einem `PhoneBook`
mit einem `Name` den zugehörigen `Entry` findet`


> usePB :: PhoneBook -> Name -> Entry
> usePB = undefined



Implementieren Sie eine `Monoid` Instanz für `PhoneBook`, um zu garantieren, 
dass `PhoneBook` ein leeres Element hat und eine Verkettungsfunktion, die 
zwei `PhoneBook`s in eines zusammenführt. 


> instance Monoid PhoneBook where 
>   mempty = undefined
>   mappend = undefined



Implementieren Sie eine Funktion `addEntry`, welche einem `PhoneBook` eine
`Name` zu `Number` Verknüpfung hinzufügt, also für einen gegebenen Namen und
eine Nummer einen Eintrag im Telefonbuch erstellt.


> addEntry :: Name -> Number -> PhoneBook -> PhoneBook 
> addEntry = undefined



Implementieren Sie eine Funktion `delEntry`, die alle Nummern aus dem `PhoneBook` 
entfernt, die mit dem gegebenen `Name` assoziiert sind. 
Hinweis: "Entfernt" heißt streng genommen nur, dass die Nummern nicht mehr aus
dem resultierenden Telefonbuch herausgesucht werden können


> delEntry :: Name -> PhoneBook -> PhoneBook
> delEntry = undefined



Implementieren Sie eine Funktion `findInMult`, welche alle Einträge aus einer
Liste von `PhoneBook`s sucht


> findInMult :: [PhoneBook] -> Name -> Entry 
> findInMult = undefined



> result = "Wie war noch mal die Nummer von diesem Alonzo Church? Vielleicht kann der mir weiterhelfen.. \n"  
>       ++ (show $ findInMult [pb2,pb3] "Alonzo Church") ++ "\n"  
>   where pb1 = addEntry "Alonzo Church" "(0123) 73645362" mempty  
>         pb2 = delEntry "Alonzo Church" pb1  
>         pb3 = addEntry "Haskell Brooks Curry" "(0167) 987761262" (mappend pb1 pb2)  




