Übungszettel 2
==============

Die Aufgaben 1 - 3 finden sie auf dem branch master in diesem Repository.

Die Aufgabe 4 liegt auf dem branch ghc-vis.

Installationsanleitung Aufgabe4
-------------------------------

Setup
> $ git checkout ghc-vis
> $ stack setup
> $ stack install gtk2hs-buildtools
> $ PATH=$HOME/.local/bin/:$PATH
> $ stack build

