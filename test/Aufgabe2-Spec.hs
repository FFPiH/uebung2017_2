main :: IO ()
main = putStrLn $ "Eine Testung von Aufgabe 2 mit HUnit ist nicht sinnvoll. \n"
               ++ "Falls Sie feststecken und nicht weiterkommen:  \n"
               ++ "Welche Typklassen kennen Sie? \n" 
               ++ "Wie sehen deren Instanzen für die vorliegenden Datentypen aus? \n"
               ++ "Welche Regeln/Gesetze implizieren diese Typklassen? \n"
               ++ "Eventuell lassen sich Ausdrücke mehrfach verallgemeinern. \n"