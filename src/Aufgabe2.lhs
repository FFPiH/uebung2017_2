Aufgabe 2
=========

> module Aufgabe2 where

Besser und allgemeiner
----------------------

Vereinfachen und verallgemeinern sie folgenden Ausdrücke so weit wie
möglich. Geben Sie die dadurch entstehenden Typsignaturen und 
Funktionsdefinitionen an. Bedenken Sie, dass wenn sie auf eine Typklasse 
abstrahieren, Sie die gesamten Gesetze der Typklasse benutzen können. 

Kann die Funktion nachher mehr als vorher?

*Bonus*: Hat sich an der Laufzeit etwas verändert?


> mystery1 :: [[a]] -> [[a]]  
> mystery1 = map (++[])  


> mystery2 :: (Int -> Bool)  
>          -> Maybe (Either String Int)  
>          -> Maybe (Either String Bool)  
> mystery2 f (Just (Right a)) = Just . Right . f $ a  
> mystery2 _ (Just (Left b))  = Just (Left b)  
> mystery2 _  Nothing         = Nothing  


> mystery3 :: (Eq a) => a -> a -> a -> Bool  
> mystery3 x y z  
>   | y == z           = True  
>   | z == y && y == x = True  
>   | x /= z           = False  
>   | y /= x           = False  
>   | z /= y || y /= x = True  
>   | otherwise        = False  



> result = "foo?"  



