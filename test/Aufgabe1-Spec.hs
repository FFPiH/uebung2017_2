import Aufgabe1

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)
import Test.HUnit


{- TEST DATA -}

data1,data2,data3 :: [(String,String)]
data1 = [("Phillip","(04165) 9876543"),("Phillip","(03548) 1234567"),("Paula","(035383) 567890")]
data2 = [("Paula","(04165) 8765432"),("Michael","(03548) 2345678"),("Ariane","(035383) 678901")]
data3 = [("Paula","(04165) 7654321"),("Hannelore","(03548) 3456789"),("Helmut","(035383) 789012")]

noData :: [(String,String)]
noData = mempty

xPB :: PhoneBook
xPB = PB $ \n -> [b|(a,b)<-data1, n==a]

yPB :: PhoneBook
yPB = PB $ \n -> [b|(a,b)<-data2, n==a]

zPB :: PhoneBook
zPB = PB $ \n -> [b|(a,b)<-data3, n==a]

examplePB = xPB

emptyPhoneBook :: PhoneBook
emptyPhoneBook = PB $ \a -> [b|(a,b)<-noData]

{- TEST CASES -}

usePBTest = testCase "Benutze beispielhaftes PhoneBook"
          $ assertEqual "usePB examplePB \"Phillip\" sollte folgende zwei Nummern rausgeben" ["(04165) 9876543","(03548) 1234567"]
          $ usePB examplePB "Phillip"

mappendTest = testCase "Assoziativität von mappend"
            $ assertEqual "Assoziativität von mappend ist nicht erfüllt" (usePB (mappend (mappend xPB yPB) zPB) "Paula")
            $ usePB (mappend xPB $ mappend yPB zPB) "Paula"

memptyTest1 = testCase "Linksidentität von mempty"
            $ assertEqual "Linksidentität von mempty ist nicht erfüllt" (usePB xPB "Phillip")
            $ usePB (mappend xPB mempty) "Phillip"

memptyTest2 = testCase "Rechtsidentität von mempty"
            $ assertEqual "Rechtsidentität von mempty ist nicht erfüllt" (usePB xPB "Phillip")
            $ usePB (mappend mempty xPB) "Phillip"

addEntryTest = testCase "Füge Name-Nummer-Verknüpfung hinzu"
             $ assertEqual "Die hinzugefügte Nummer wird nicht gefunden" ["12345"]
             $ usePB (addEntry "NeuerName" "12345" emptyPhoneBook) "NeuerName"

delEntryTest = testCase "Lösche Name-Nummer-Verknüpfung"
             $ assertEqual "Die gelöschte Nummer wird weiterhin gefunden" []
             $ usePB (delEntry "Paula" xPB) "Paula"

findInMultTest = testCase "Suche in mehreren PhoneBooks"
               $ assertEqual "Es werden nicht alle drei Nummern von Paula gefunden" ["(035383) 567890","(04165) 8765432","(04165) 7654321"]
               $ findInMult [xPB,yPB,zPB] "Paula"


tests = [usePBTest,mappendTest,memptyTest1,memptyTest2,addEntryTest,delEntryTest,findInMultTest]

main :: IO ()
main = defaultMain tests
