import Aufgabe3
import DataPB
import FunPB
import AreaCode

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)
import Test.HUnit


{- TEST DATA -}

funPBTest = dataToFunPB simpleData

vielleichtValues = [(Etwas 1234),Nichts]

entwederValues :: [Entweder String [[[Integer]]]]
entwederValues = [(Jenes "just a string"),(Dieses [[[12344321]]])]

konstantValue = Konstant True

f = const 1
g = const '0'


{- TEST CASES-}

atleast5CharFalse = testCase "Teste Randbedingung (==False) für atleast5Char"
  $ assertEqual "Der Ausdruck >>> runPred atLeast5Char $ \"1234\" \nsollte zu False auswerten" False
  $ runPred atLeast5Char $ "1234"

atleast5CharTrue = testCase "Teste Randbedingung (==True) für atleast5Char"
  $ assertEqual "Der Ausdruck >>> runPred atLeast5Char $ \"12345\" \nsollte zu True auswerten" True
  $ runPred atLeast5Char $ "12345"



fmapVielleichtTestIdentity = testCase "Functor Vielleicht: Strukturerhaltung"
  $ assertEqual "Es sollte gelten: fmap id == id . " (id <$> vielleichtValues)
  $ ((fmap id) <$> vielleichtValues)

fmapVielleichtTestComposability = testCase "Functor Vielleicht: Komponierbarkeit"
  $ assertEqual "Es sollte gelten: fmap f . fmap g == fmap (f . g) . " ((fmap f . fmap g) <$> vielleichtValues)
  $ ((fmap (f . g)) <$> vielleichtValues)


fmapEntwederTestIdentity = testCase "Functor (Entweder a): Strukturerhaltung"
  $ assertEqual "Es sollte gelten: fmap id == id . " (id <$> entwederValues)
  $ ((fmap id) <$> entwederValues)

fmapEntwederTestComposability = testCase "Functor (Entweder a): Komponierbarkeit"
  $ assertEqual "Es sollte gelten: fmap f . fmap g == fmap (f . g) . " ((fmap f . fmap g) <$> entwederValues)
  $ ((fmap (f . g)) <$> entwederValues)


fmapKonstantTestIdentity = testCase "Functor (Konstant a): Strukturerhaltung"
  $ assertEqual "Es sollte gelten: fmap id == id . " (id konstantValue)
  $ ((fmap id) konstantValue)

fmapKonstantTestComposability = testCase "Functor (Konstant a): Komponierbarkeit"
  $ assertEqual "Es sollte gelten: fmap f . fmap g == fmap (f . g) . " ((fmap f . fmap g) konstantValue)
  $ ((fmap (f . g)) konstantValue)



fmapFunPBTestIdentity = testCase "Functor (FunPB a): Strukturerhaltung"
  $ assertEqual "Es sollte gelten: fmap id == id . " (runFunPB (id funPBTest) "Paula")
  $ (runFunPB ((fmap id) funPBTest) "Paula")

fmapFunPBTestComposability = testCase "Functor (FunPB a): Komponierbarkeit"
  $ assertEqual "Es sollte gelten: fmap f . fmap g == fmap (f . g) . " (runFunPB (fmap snd . fmap separateAreaCode $ funPBTest) "Paula")
  $ (runFunPB ((fmap (snd.separateAreaCode)) funPBTest) "Paula")

tests = [atleast5CharFalse,atleast5CharTrue,fmapVielleichtTestIdentity,fmapVielleichtTestComposability,fmapEntwederTestIdentity,fmapEntwederTestComposability,fmapKonstantTestIdentity,fmapKonstantTestComposability,fmapFunPBTestIdentity,fmapFunPBTestComposability]

main :: IO ()
main = defaultMain tests