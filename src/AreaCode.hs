module AreaCode (AreaCode,separateAreaCode) where

import qualified Data.Attoparsec.Text as A  
import Data.Attoparsec.Combinator  
import qualified Data.Text as T 

type Number = String  
type Name = String  
type AreaCode = String

{-Area Code Parser-}

separateAreaCode :: Number -> (AreaCode,Number)  
separateAreaCode nr = 
  case A.parse parseAreaCode (T.pack nr) of 
       A.Done s ac -> (ac, T.unpack s)
       _           -> ("",nr)

parseAreaCode :: A.Parser String  
parseAreaCode = do  
  many' (A.char ' ')  
  A.char '('  
  ac <- many1 $ A.satisfy (A.inClass "0123456789")
  A.char ')'  
  many' (A.char ' ')  
  return ac

